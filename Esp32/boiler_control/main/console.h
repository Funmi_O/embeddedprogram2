/*
 * console.h
 *
 *  Created on: Nov 23, 2020
 *      Author: funmilayo75
 */

#ifndef MAIN_CONSOLE_H_
#define MAIN_CONSOLE_H_

#define CONSOLE_TASK_SIZE (4096)

#define CONSOLE_HIST_LINE_SIZE (100)

void console_init();

#endif /* MAIN_CONSOLE_H_ */
