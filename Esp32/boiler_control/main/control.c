/*
 * control.c
 *
 *  Created on: Dec 22, 2020
 *      Author: funmilayo75
 */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "analog.h"
#include "control.h"
#include "temperature.h"

#include "time.h"
#include "esp_err.h"
#include "esp_console.h"
#include "argtable3/argtable3.h"
#include "freertos/FreeRTOS.h"

#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE
//#define LOG_LOCAL_LEVEL ESP_LOG_ERROR
#include "esp_log.h"

#define TAG "control"

#define MIN_ON_TIME (1500)
#define MIN_OFF_TIME (1500)

#define MAX_CHANNELS (2)
#define TEMP_DIFFERENCE (5)
#define TB_CHANNEL (0)
#define TF_CHANNEL (1)

enum Pump {OFF , ON , STAY};

typedef struct {
	float temp_a;
	float temp_b;
	uint8_t pump_state;
	uint16_t current_AD_value;
	time_t now;
	time_t last_on;
} logic_control_t;

logic_control_t logic_control;


uint16_t check_dig_input() {
	ESP_LOGD(TAG, "Digital Input for temperature ");

	uint16_t dig_input=analog_get(&logic_control.current_AD_value, ((TickType_t) 30) == pdTRUE);

	ESP_LOGI(TAG, "dig_input: %d", dig_input);
	return logic_control.current_AD_value;
}

uint8_t control_logic(){
	time(&logic_control.last_on);

	logic_control.temp_a = temperature_get(TB_CHANNEL, logic_control.current_AD_value);
	logic_control.temp_b= temperature_get(TF_CHANNEL, logic_control.current_AD_value);

	time(&logic_control.now);

	if (logic_control.temp_b - logic_control.temp_a > TEMP_DIFFERENCE) {
			if ((logic_control.now-logic_control.last_on) < MIN_ON_TIME) {
				return OFF;
			}
			else {
				return ON;
			}
	}

	if ((logic_control.temp_b - logic_control.temp_a) < TEMP_DIFFERENCE) {
			if ((logic_control.now-logic_control.last_on) < MIN_OFF_TIME) {
				return ON;
			}
			else {
				return OFF;
			}
		}

	return STAY;
}

void pump_control() {
	ESP_LOGD(TAG, "Check Pump state ");

	if (control_logic()==ON) {
		logic_control.pump_state=ON;
		return;
	}
	if (control_logic()==OFF) {
		logic_control.pump_state=OFF;
		return;
	}
	return;
}


#define CMD_PUMP "pump"
#define HELP_PUMP "check state of the pump"
#define HINT_PUMP "you have to choose the state of the pump through the temperature difference"

#define ARG_HELP "help"
#define HINT_HELP "hint for help"

#define ARG_SHOW "show"
#define HINT_SHOW "hint for show"

static struct {
	struct arg_lit *help;
	struct arg_lit *show;
	struct arg_end *end;
} pump_args;

static int cmd_pump(int argc, char **argv) {
	int nerrors=arg_parse(argc, argv, (void **) &pump_args);
	if (nerrors || argc==1 || pump_args.help->count>0) {
		printf(HELP_PUMP);
		return 0;
	}

	if (pump_args.show->count==0) {				//Show
			printf("Pump_state is %d, It has been on since: %ld\n",
					logic_control.pump_state, logic_control.last_on);
			return 0;
	}

	return 1;
}

static void register_cmd() {
	pump_args.help=arg_lit0("hH", ARG_HELP, HINT_HELP);
	pump_args.show=arg_litn("sS", ARG_SHOW, 0, 1, HINT_SHOW);
	pump_args.end=arg_end(0);

	const esp_console_cmd_t cmd = {
			.command=CMD_PUMP,
			.help=HELP_PUMP,
			.hint=HINT_PUMP,
			.func=&cmd_pump
	};

	ESP_ERROR_CHECK(esp_console_cmd_register(&cmd));
}

void control_init() {
	bzero(&logic_control, sizeof(logic_control_t));

	register_cmd();
	pump_control();

}

BaseType_t control_deinit() {
	return pdPASS;
}
