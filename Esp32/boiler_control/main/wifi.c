/*
 * wifi.c
 *
 *  Created on: Dec 31, 2020
 *      Author: funmilayo75
 */

#include <stdint.h>
#include <string.h>
#include "esp_http_server.h"
#include "esp_system.h"
#include "cJSON.h"
#include "freertos/task.h"

#include "wifi.h"

#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE
//#define LOG_LOCAL_LEVEL ESP_LOG_ERROR
#include "esp_log.h"


#define TAG "webserver"

#define URI_IS_NOT_AVAILABLE "URI is not available"
#define URI_STAT "/stat"
#define URI_CHANNEL_0 "/temp0"
#define URI_CHANNEL_1 "/temp1"

#define MIME_JSON "text/json"

#define BUFFER_SIZE 1024

#define CHANNELS (2)

typedef struct {
	uint8_t valid;
	uint16_t d0;
	uint16_t d1;
	float t0;
	float t1;
	float tg_alpha;
} verification_t;

typedef struct {
	uint16_t raw;
	uint16_t normalized;
	float temperature;
} channel_data_t;

typedef struct {
	verification_t verification;
	channel_data_t channel_data;
} data_t;

static httpd_handle_t server=NULL;
static char buffer[BUFFER_SIZE];
static data_t data[CHANNELS];
static TaskHandle_t bg_task_handler;

static char *get_params_as_channel_json(int i, char *buffer, size_t buffer_length) {
	cJSON *json=cJSON_CreateObject();
	cJSON *cd=cJSON_CreateObject();

			if (!(cJSON_AddNumberToObject(cd, "raw", data[i].channel_data.raw)))
				goto exit;

			if (!(cJSON_AddNumberToObject(cd, "normalized", data[i].channel_data.normalized)))
				goto exit;

			if (!(cJSON_AddNumberToObject(cd, "temperature", data[i].channel_data.temperature)))
				goto exit;

			cJSON *v=cJSON_CreateObject();
			if (!v)
				goto exit;

			if (!(cJSON_AddNumberToObject(v, "valid", data[i].verification.valid)))
				goto exit;

			if (!(cJSON_AddNumberToObject(v, "d0", data[i].verification.d0)))
				goto exit;

			if (!(cJSON_AddNumberToObject(v, "d1", data[i].verification.d1)))
				goto exit;

			if (!(cJSON_AddNumberToObject(v, "t0", data[i].verification.t0)))
				goto exit;

			if (!(cJSON_AddNumberToObject(v, "t1", data[i].verification.t1)))
				goto exit;

			if (!(cJSON_AddNumberToObject(v, "tg_alpha", data[i].verification.tg_alpha)))
				goto exit;

			cJSON *parent=cJSON_CreateObject();

			if (!parent)
				goto exit;

			cJSON_AddItemToObject(parent, "Channel data", cd);
			cJSON_AddItemToObject(parent, "Verification", v);
			cJSON_AddItemToObject(json, "Channel", parent);

			cJSON_PrintPreallocated(json, buffer, buffer_length, true);

	exit:
		cJSON_Delete(json);
		return buffer;
}

static char *get_params_as_json(char *buffer, size_t buffer_length) {
	cJSON *json=cJSON_CreateObject();
	cJSON *entries;
	if (!(entries=cJSON_AddArrayToObject(json, "entries")))
		goto exit;

	for(int i=0;i<CHANNELS;++i) {
		cJSON *cd=cJSON_CreateObject();
		if (!(cJSON_AddNumberToObject(cd, "raw", data[i].channel_data.raw)))
			goto exit;

		if (!(cJSON_AddNumberToObject(cd, "normalized", data[i].channel_data.normalized)))
			goto exit;

		if (!(cJSON_AddNumberToObject(cd, "temperature", data[i].channel_data.temperature)))
			goto exit;

		cJSON *v=cJSON_CreateObject();
		if (!v)
			goto exit;

		if (!(cJSON_AddNumberToObject(v, "valid", data[i].verification.valid)))
			goto exit;

		if (!(cJSON_AddNumberToObject(v, "d0", data[i].verification.d0)))
			goto exit;

		if (!(cJSON_AddNumberToObject(v, "d1", data[i].verification.d1)))
			goto exit;

		if (!(cJSON_AddNumberToObject(v, "t0", data[i].verification.t0)))
			goto exit;

		if (!(cJSON_AddNumberToObject(v, "t1", data[i].verification.t1)))
			goto exit;

		if (!(cJSON_AddNumberToObject(v, "tg_alpha", data[i].verification.tg_alpha)))
			goto exit;

		cJSON *parent=cJSON_CreateObject();

		if (!parent)
			goto exit;

		cJSON_AddItemToObject(parent, "Channel data", cd);
		cJSON_AddItemToObject(parent, "Verification", v);
		cJSON_AddItemToArray(entries, parent);
	}

	cJSON_PrintPreallocated(json, buffer, buffer_length, true);

exit:
	cJSON_Delete(json);
	return buffer;
}

static esp_err_t stat_handler(httpd_req_t *req) {
	if (!req)
		return ESP_ERR_HTTPD_INVALID_REQ;
	httpd_resp_set_type(req, MIME_JSON);
	httpd_resp_send(req, get_params_as_json(buffer, BUFFER_SIZE), strlen(buffer));
	return ESP_OK;
}

static esp_err_t ch0_handler(httpd_req_t *req) {
	if (!req)
		return ESP_ERR_HTTPD_INVALID_REQ;
	httpd_resp_set_type(req, MIME_JSON);
	httpd_resp_send(req, get_params_as_channel_json(0, buffer, BUFFER_SIZE), strlen(buffer));
	return ESP_OK;
}

static esp_err_t ch1_handler(httpd_req_t *req) {
	if (!req)
		return ESP_ERR_HTTPD_INVALID_REQ;
	httpd_resp_set_type(req, MIME_JSON);
	httpd_resp_send(req, get_params_as_channel_json(1, buffer, BUFFER_SIZE), strlen(buffer));
	return ESP_OK;
}

static const httpd_uri_t status = {
		.uri=URI_STAT,
		.method=HTTP_GET,
		.handler=stat_handler,
		.user_ctx=NULL
};

static const httpd_uri_t ch0 = {
		.uri=URI_CHANNEL_0,
		.method=HTTP_GET,
		.handler=ch0_handler,
		.user_ctx=NULL
};

static const httpd_uri_t ch1 = {
		.uri=URI_CHANNEL_1,
		.method=HTTP_GET,
		.handler=ch1_handler,
		.user_ctx=NULL
};

static void fn_bg(void *param) {
	verification_t *v=&data[0].verification;
	v->d0=0;
	v->d1=4096;
	v->t0=20.0;
	v->t1=30.0;
	v->tg_alpha=(v->t1-v->t0)/(v->d1-v->d0);
	v->valid=1;

	v=&data[1].verification;
	v->d0=10;
	v->d1=3900;
	v->t0=25.0;
	v->t1=29.0;
	v->tg_alpha=(v->t1-v->t0)/(v->d1-v->d0);
	v->valid=1;

	for(;;) {
		for (int i=0;i<CHANNELS;++i) {
			channel_data_t *cd=&data[i].channel_data;
			v=&data[i].verification;
			cd->raw=(cd->raw+10) % 4096;
			cd->normalized=cd->raw;
			cd->temperature=v->t0+(cd->normalized-v->d0)*v->tg_alpha;
		}
		vTaskDelay(pdMS_TO_TICKS(500));
	}
}

BaseType_t ws_init() {
	ESP_LOGD(TAG, "ws_init ");
	bzero(&data, sizeof(data));
	httpd_config_t config=HTTPD_DEFAULT_CONFIG();
	ESP_LOGI(TAG, "Starting server on port:%d", config.server_port);
	esp_err_t err=httpd_start(&server, &config);
	if (err==ESP_OK) {
		ESP_LOGI(TAG, "Registering URI handlers ");
		httpd_register_uri_handler(server, &status);
		httpd_register_uri_handler(server, &ch0);
		httpd_register_uri_handler(server, &ch1);
		xTaskCreate(fn_bg, "webserver", 1024, NULL, uxTaskPriorityGet(NULL), bg_task_handler);
		return pdPASS;
	}
	ESP_LOGE(TAG, "Error starting server");
	return pdFAIL;
}

BaseType_t ws_deinit() {
	return pdPASS;
}

