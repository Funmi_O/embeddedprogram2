/*
 * temperature.h
 *
 *  Created on: Nov 28, 2020
 *      Author: funmilayo75
 */

#ifndef MAIN_TEMPERATURE_H_
#define MAIN_TEMPERATURE_H_

#include "freertos/FreeRTOS.h"

BaseType_t temperature_init();

BaseType_t temperature_deinit();

float temperature_get(uint8_t ch, uint16_t dig_input);


#endif /* MAIN_TEMPERATURE_H_ */
