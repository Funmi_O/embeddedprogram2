/*
 * temperature.c
 *
 *  Created on: Nov 28, 2020
 *      Author: funmilayo75
 */
#include <stdint.h>
#include <strings.h>

#include "esp_console.h"
#include "argtable3/argtable3.h"

#include "temperature.h"

#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE
//#define LOG_LOCAL_LEVEL ESP_LOG_ERROR
#include "esp_log.h"


#define TAG "temperature"

#define MIN_TEMPERATURE 0
#define MAX_TEMPERATURE 100

#define MIN_DIGITAL 0
#define MAX_DIGITAL 4096

#define MAX_CHANNEL (2)

typedef struct {
	uint8_t verified;
	uint16_t d0;
	uint16_t d1;
	float t0;
	float t1;
	float tg_alpha;
} temp_verification_t;

static temp_verification_t temp_verification[MAX_CHANNEL];

//cmd interface temp <-c channel> [help] <set|get> [min|max]

static inline int check_channel(uint8_t ch) {
	if (ch>=MAX_CHANNEL) {
		ESP_LOGE(TAG, "channel maximum value is %d", MAX_CHANNEL);
		return 0;
	}

	return 1;
}

static void calc_tg_alpha(uint8_t ch) {
	ESP_LOGD(TAG, "Enter calc_tg_alpha channel:%d", ch);
	if (!check_channel(ch))
		return;
	if (temp_verification[ch].d1>temp_verification[ch].d0 &&
		temp_verification[ch].t1>temp_verification[ch].t0) {
		temp_verification[ch].tg_alpha=(temp_verification[ch].t1-temp_verification[ch].t0) /
					((float)(temp_verification[ch].d1-temp_verification[ch].d0));
		temp_verification[ch].verified=1;
	}
}

static void verify_set_min (uint8_t channel, float temp) {
	ESP_LOGD(TAG, "Enter verify_set_min_channel:%d, temp:%f", channel, temp);
	if (!check_channel(channel))
		return;

	temp_verification[channel].d0=MIN_DIGITAL;
	temp_verification[channel].t0=temp;
	calc_tg_alpha(channel);
}

static void verify_set_max (uint8_t channel, float temp) {
	ESP_LOGD(TAG, "Enter verify_set_max_channel:%d, temp:%f", channel, temp);
	if (!check_channel(channel))
		return;

	temp_verification[channel].d1=MAX_DIGITAL;
	temp_verification[channel].t1=temp;
	calc_tg_alpha(channel);
}

float temperature_get(uint8_t ch, uint16_t dig_input) {
	ESP_LOGD(TAG, "Enter temperature_get, ch:%d, input is %d", ch, dig_input);
	if(!check_channel(ch))
		return 0.0f;
	if (!temp_verification[ch].verified) {
		printf("channel %d is not verified yet\n", ch);
		return 0.0f;
	}

	return temp_verification[ch].t0 +
			(dig_input-temp_verification[ch].d0) * temp_verification[ch].tg_alpha;
}

#define CMD_TEMP "temp"
#define HELP_TEMP "set verification parameters of the given channel"
#define HINT_TEMP "you have to set min and max points which are far enough to each other"

#define ARG_HELP "help"
#define HINT_HELP "hint for help"

#define ARG_SET "set"
#define HINT_SET "hint for set"

#define ARG_GET "get"
#define HINT_GET "hint for get"

#define ARG_MIN "min"
#define HINT_MIN "hint for min"

#define T_ARG_MAX "max"
#define HINT_MAX "hint for max"

#define ARG_CHANNEL "channel"
#define HINT_CHANNEL "hint for channel"

#define DATA_TYPE_INT "<n>"

static struct {
	struct arg_lit *help;
	struct arg_lit *set;
	struct arg_int *get;
	struct arg_int *min;
	struct arg_int *max;
	struct arg_int *channel;
	struct arg_end *end;
} temp_args;

static int cmd_temp(int argc, char **argv) {
	int nerrors=arg_parse(argc, argv, (void **) &temp_args);
	if (nerrors || argc==1 || temp_args.help->count>0) {
		printf(HELP_TEMP);
		return 0;
	}

	if (temp_args.set->count>0 && temp_args.get->count>0) {
		printf("Set or Get?\n");
		return 1;
	}

	if (temp_args.channel->count==0) {
			printf("You always have to define channel\n");
			return 1;
	}

	uint8_t ch = temp_args.channel->ival[0];

	if (temp_args.set->count>0) {//set
		if (temp_args.min->count>0)  {//min
			verify_set_min(ch, temp_args.min->ival[0]);
			return 0;
		}

		if (temp_args.max->count>0)  {//min
			verify_set_max(ch, temp_args.max->ival[0]);
			return 0;
		}

		return 1;
	}

	if (temp_args.get->count>0) { //get
		printf("temperature belongs to %d is %f Celsius\n",
			ch,
			temperature_get(ch, temp_args.get->ival[0]));
		return 0;
	}

	return 1;
}

static void register_cmd() {
	temp_args.help=arg_lit0("hH", ARG_HELP, HINT_HELP);
	temp_args.set=arg_litn("sS", ARG_SET, 0, 1, HINT_SET);
	temp_args.get=arg_intn("gG", ARG_GET, DATA_TYPE_INT, 0, 1, HINT_GET);
	temp_args.min=arg_intn("nN", ARG_MIN, DATA_TYPE_INT, 0, 1, HINT_MIN);
	temp_args.max=arg_intn("xX", T_ARG_MAX, DATA_TYPE_INT, 0, 1, HINT_MAX);
	temp_args.channel=arg_intn("cC", ARG_CHANNEL, DATA_TYPE_INT, 0, 1, HINT_CHANNEL);
	temp_args.end=arg_end(0);

	const esp_console_cmd_t cmd = {
			.command=CMD_TEMP,
			.help=HELP_TEMP,
			.hint=HINT_TEMP,
			.func=&cmd_temp
	};

	ESP_ERROR_CHECK(esp_console_cmd_register(&cmd));
}

BaseType_t temperature_init() {
	bzero(temp_verification, sizeof(temp_verification));
	register_cmd();
	return pdPASS;
}

BaseType_t temperature_deinit() {
	return pdPASS;
}


