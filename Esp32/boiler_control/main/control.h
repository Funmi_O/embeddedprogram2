/*
 * control.h
 *
 *  Created on: Dec 22, 2020
 *      Author: funmilayo75
 */

#ifndef MAIN_CONTROL_H_
#define MAIN_CONTROL_H_

#include <stdint.h>
#include "freertos/FreeRTOS.h"

void control_init();

BaseType_t control_deinit();

#endif /* MAIN_CONTROL_H_ */
