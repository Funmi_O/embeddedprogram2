/*
 * wifi.h
 *
 *  Created on: Dec 31, 2020
 *      Author: funmilayo75
 */

#ifndef MAIN_WIFI_H_
#define MAIN_WIFI_H_

#include "freertos/FreeRTOS.h"

BaseType_t ws_init();

BaseType_t ws_deinit();



#endif /* MAIN_WIFI_H_ */
