/*
 * flashing.c
 *
 *  Created on: Nov 30, 2020
 *      Author: funmilayo75
 */

#include <stdint.h>
#include <string.h>
#include "flashing.h"
#include "nvs.h"
#include "esp_console.h"
#include "argtable3/argtable3.h"


#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE
//#define LOG_LOCAL_LEVEL ESP_LOG_ERROR
#include "esp_log.h"


#define TAG "flashing"

#define KEY_PARAM_BYTE "pb"
#define KEY_PARAM_WORD "pw"
#define KEY_PARAM_D_WORD "pdw"
#define KEY_PARAM_FLOAT "pf"
#define KEY_PARAM_DOUBLE "pd"
#define KEY_PARAM_STRING "ps"

#define DEF_PARAM_BYTE ((uint8_t)42)
#define DEF_PARAM_WORD ((uint16_t)43)
#define DEF_PARAM_D_WORD ((uint32_t)44)
#define DEF_PARAM_FLOAT ((float)42.0)
#define DEF_PARAM_DOUBLE ((float)43.434343)
#define DEF_PARAM_STRING "Zaphod Beeblebrox"

#define PARAM_STRING_MAX_LEN (50)

#define MIN_PARAM_BYTE ((uint8_t)0)
#define MIN_PARAM_WORD ((uint16_t)10)
#define MIN_PARAM_D_WORD ((uint32_t)30)
#define MIN_PARAM_FLOAT ((float)10.0)
#define MIN_PARAM_DOUBLE ((double)16.0)

#define MAX_PARAM_BYTE ((uint8_t)250)
#define MAX_PARAM_WORD ((uint16_t)50000)
#define MAX_PARAM_D_WORD ((uint32_t)1000000)
#define MAX_PARAM_FLOAT ((float)1000.0)
#define MAX_PARAM_DOUBLE ((double)1600000.0)

typedef struct {
	uint8_t param_byte;
	uint16_t param_word;
	uint32_t param_d_word;
	float param_float;
	double param_double;
	char *param_string;
	uint8_t valid;
	uint8_t need_save;
} persistent_data_t;

static persistent_data_t persistent_data;

#define LOAD_KEY_NUMBER(handler, key, func, cast, value, def, min, max) 	\
	do {																	\
		esp_err_t err=func((handler), (key), cast &(value));				\
		if (err!=ESP_OK || (value) < (min) || (value) > (max))				\
		(value) = (def);													\
	} while(0)


#define LOAD_KEY_FI(handler, key, func, cast, value, divider, def, min, max) 	\
	do {																		\
		int fi = 0;																\
		esp_err_t err=func((handler), (key), &fi);								\
		double fd=fi/(double)(divider);											\
		(value)=(err==ESP_OK && fd>=(min) && fd<=(max)) ? fd : (def);			\
	} while(0)

#define LOAD_KEY_STR(handler, key, value, def) 							\
	do {																\
		size_t sl;														\
		esp_err_t err=nvs_get_str(handler, key, NULL, &sl);				\
		if (err==ESP_OK && sl>0){										\
			value=malloc(sl);											\
			err= nvs_get_str(handler, key, value, &sl);					\
			if (err!=ESP_OK)											\
				value=strdup(def);										\
		}																\
	} while(0)

static BaseType_t load_params() {
	ESP_LOGD(TAG, "load_params");
	nvs_handle_t nvs_handler;
	esp_err_t err=nvs_open(TAG, NVS_READONLY, &nvs_handler);
	if (err!=ESP_OK) {
		ESP_LOGE(TAG, "Error opening flash: %d", err);
		return pdFAIL;
	}

	LOAD_KEY_NUMBER(nvs_handler, KEY_PARAM_BYTE, nvs_get_i8, (int8_t *), persistent_data.param_byte,
			DEF_PARAM_BYTE, MIN_PARAM_BYTE, MAX_PARAM_BYTE);

	LOAD_KEY_NUMBER(nvs_handler, KEY_PARAM_WORD, nvs_get_i16, (int16_t *), persistent_data.param_word,
			DEF_PARAM_WORD, MIN_PARAM_WORD, MAX_PARAM_WORD);

	LOAD_KEY_NUMBER(nvs_handler, KEY_PARAM_D_WORD, nvs_get_i32, (int32_t *), persistent_data.param_d_word,
			DEF_PARAM_D_WORD, MIN_PARAM_D_WORD, MAX_PARAM_D_WORD);

	LOAD_KEY_FI(nvs_handler, KEY_PARAM_FLOAT, nvs_get_i32, (int32_t *), persistent_data.param_float, 100,
			DEF_PARAM_FLOAT, MIN_PARAM_FLOAT, MAX_PARAM_FLOAT);

	LOAD_KEY_FI(nvs_handler, KEY_PARAM_DOUBLE, nvs_get_i32, (int32_t *), persistent_data.param_double, 100,
				DEF_PARAM_DOUBLE, MIN_PARAM_DOUBLE, MAX_PARAM_DOUBLE);

	LOAD_KEY_STR(nvs_handler, KEY_PARAM_STRING, persistent_data.param_string, DEF_PARAM_STRING);

	nvs_close(nvs_handler);
	return pdTRUE;
}

#define STORE_KEY_NUMBER(handler, key, func, value) 				\
	do { 															\
		err=func(handler, key, value);								\
		if (err!=ESP_OK) {											\
			ESP_LOGE(TAG, "cannot store %s, error %d", key, err);	\
			goto error_exit;										\
		}															\
	} while(0)

#define STORE_KEY_STR(handler, key, value)							\
	do {															\
		esp_err_t err=nvs_set_str(handler, key, value);				\
		if (err!=ESP_OK) {											\
			ESP_LOGE(TAG, "cannot store %s err:%d", key, err);		\
			goto error_exit; 										\
		}															\
		free(value);												\
		size_t sl; 													\
		err=nvs_get_str(handler, key, NULL, &sl);					\
		if (err!=ESP_OK || sl==0)									\
			goto error_exit;										\
	} while(0)

static BaseType_t store_params() {
	ESP_LOGD(TAG, "store_params");
	if (!persistent_data.need_save)
		return pdTRUE;

	nvs_handle_t nvs_handler;
	esp_err_t err=nvs_open(TAG, NVS_READONLY, &nvs_handler);
	if (err!=ESP_OK) {
		ESP_LOGE(TAG, "cannot open flash: %d", err);
		return pdFAIL;
	}

	STORE_KEY_NUMBER(nvs_handler, KEY_PARAM_BYTE, nvs_set_i8, persistent_data.param_byte);
	STORE_KEY_NUMBER(nvs_handler, KEY_PARAM_WORD, nvs_set_i16, persistent_data.param_word);
	STORE_KEY_NUMBER(nvs_handler, KEY_PARAM_D_WORD, nvs_set_i32, persistent_data.param_d_word);
	STORE_KEY_NUMBER(nvs_handler, KEY_PARAM_FLOAT, nvs_set_i32, persistent_data.param_float*100);
	STORE_KEY_NUMBER(nvs_handler, KEY_PARAM_DOUBLE, nvs_set_i32, persistent_data.param_double*100);
	STORE_KEY_STR(nvs_handler, KEY_PARAM_STRING, persistent_data.param_string);

	err=nvs_commit(nvs_handler);
	if (err!=ESP_OK) {
		ESP_LOGE(TAG, "Cannot commit, err: %d", err);
		goto error_exit;
	}


	nvs_close(nvs_handler);
	return pdTRUE;

error_exit:
	nvs_close(nvs_handler);
	return pdFAIL;
}

// flash <set <byte|word|dword|float|double|str>|show|store|load|erase <<byte|word|dword|float|double|str|all>>

#define CMD_FLASH "flash"
#define HINT_FLASH "set/show/erase/load/store persistent data"
#define HELP_FLASH "flash command"	\
				   " Example:\n"	\
				   " flash --set --byte 42\n"	\
				   " flash --show\n"			\
				   " flash --load\n"			\
				   " flash --store\n"



#define ARG_SET "set"
#define HINT_SET "set a parameter"

#define ARG_SHOW "show"
#define HINT_SHOW "show all parameters"

#define ARG_STORE "store"
#define HINT_STORE "store all to flash"

#define ARG_LOAD "load"
#define HINT_LOAD "load all parameters from flash"

#define ARG_STAT "stat"
#define HINT_STAT "state of flash memory"

#define ARG_ERASE "erase"
#define HINT_ERASE "erase all parameters from flash"

#define ARG_BYTE "byte"
#define HINT_BYTE "set a byte parameter"

#define ARG_WORD "word"
#define HINT_WORD "set a word parameter"

#define ARG_D_WORD "dword"
#define HINT_D_WORD "set a dword parameter"

#define ARG_FLOAT "float"
#define HINT_FLOAT "set a float parameter"

#define ARG_DOUBLE "dbl"
#define HINT_DOUBLE "set a double parameter"

#define ARG_STRING "string"
#define HINT_STRING "set a string parameter"

#define ARG_ALL "all"
#define HINT_ALL "erase all flash"

#define DATA_TYPE_INT "<int>"
#define DATA_TYPE_FLOAT "<float>"
#define DATA_TYPE_DOUBLE "<double>"
#define DATA_TYPE_STRING "<string>"
#define DATA_TYPE_ERASE "<key|all>"

static struct {
	struct arg_lit *set;
	struct arg_lit *show;
	struct arg_lit *store;
	struct arg_lit *load;
	struct arg_lit *stat;
	struct arg_str *erase;
	struct arg_int *byte;
	struct arg_int *word;
	struct arg_int *dword;
	struct arg_dbl *fl;
	struct arg_dbl *dbl;
	struct arg_str *str;
	struct arg_end *end;
} flash_args;

static void print_flash_stat() {
	ESP_LOGD(TAG, "print_flash_stat ");
	nvs_stats_t nvs_stat;
	nvs_get_stats(TAG, &nvs_stat);
	printf("Count: Used entries:%d, Free entries:%d, all entries:%d\n",
			nvs_stat.used_entries, nvs_stat.free_entries, nvs_stat.total_entries);
}

static void erase_all() {
	ESP_LOGD(TAG, "erase_all ");
	nvs_handle_t handler;
	esp_err_t err=nvs_open(TAG, NVS_READWRITE, &handler);
	if (err!=ESP_OK) {
		ESP_LOGE(TAG, "error opening flash err:%d", err);
		return;
	}

	err=nvs_erase_all(handler);
	if(err!=ESP_OK) {
		ESP_LOGE(TAG, "cannot erase all:%d", err);
		goto exit;
	}

	err=nvs_commit(handler);
	if (err!=ESP_OK) {
		ESP_LOGE(TAG, "cannot commit erase:%d", err);
		goto exit;
	}

	printf("Flash erased successfully\n");

exit:
	nvs_close(handler);
}

static void erase_key(const char *key) {
	ESP_LOGD(TAG, "erase_key : %s", key);
	if (!key)
		return;

	nvs_handle_t handler;
	esp_err_t err=nvs_open(TAG, NVS_READWRITE, &handler);
	if (err!=ESP_OK) {
		ESP_LOGE(TAG, "cannot open flash: %d", err);
		return;
	}

	nvs_iterator_t it=nvs_entry_find(TAG, key, NVS_TYPE_ANY);
	if (!it) {
		ESP_LOGW(TAG, "Cannot find key:%s", key);
		goto exit;
	}

	err=nvs_erase_key(handler, key);
	if (err!=ESP_OK) {

	}

	err=nvs_commit(handler);
	if (err!=ESP_OK) {
		ESP_LOGW(TAG, "cannot commit erase entry:%s, err:%d", key, err);
		goto exit;
	}

	printf("Key %s is erased\n", key);

exit:
	nvs_close(handler);
}

#define SET_PARAM_NUMERIC(v, persistent, min, max)  	\
	do {												\
		if (v>=min && v<=max) {							\
			persistent=v;								\
			persistent_data.need_save=1;				\
		}												\
	} while(0)


#define SET_PARAM_TEXT(v, persistent, maxlen)		\
		do { 										\
			if (!(v&&*v&&strlen(v)<maxlen))			\
				return 1;							\
			free(persistent);						\
			persistent=strdup(v);					\
			persistent_data.need_save=1;			\
			return 0;								\
		} while(0)

static int cmd_flash(int argc, char **argv) {
	ESP_LOGD(TAG, "cmd_flash ");
	int nerr=arg_parse(argc, argv,(void **) &flash_args);

	if (nerr || argc==1) {
		printf(HELP_FLASH);
		return 1;
	}

	if (flash_args.show->count>0) {
		printf("byte:%d, word:%d, dword:%d, float:%f, double:%f, string:%s\n valid:%d, need to store:%d\n",
				persistent_data.param_byte, persistent_data.param_word,
				persistent_data.param_d_word, persistent_data.param_float,
				persistent_data.param_double, persistent_data.param_string,
				persistent_data.valid, persistent_data.need_save);
		return 0;
	}

	if (flash_args.store->count>0) {  //store
		printf("storing to flash is %s\n", store_params()==pdPASS ? "successfully": "failed");
		return 0;
	}

	if (flash_args.load->count>0) {  //load
			printf("loading from flash is %s\n", load_params()==pdPASS ? "successfully": "failed");
			return 0;
	}

	if (flash_args.stat->count>0) {  //stat
			print_flash_stat();
			return 0;
	}

	if (flash_args.erase->count>0) {  //erase
		if (strcasecmp(ARG_ALL, flash_args.erase->sval[0])==0) //all
			erase_all();
		else
			erase_key(flash_args.erase->sval[0]);

		return 0;
	}

	if (flash_args.set->count>0) { //set
		if (flash_args.byte->count>0) { //byte
			SET_PARAM_NUMERIC(flash_args.byte->ival[0], persistent_data.param_byte, MIN_PARAM_BYTE, MAX_PARAM_BYTE);
			return 0;
		}

		if (flash_args.word->count>0) { //word
			SET_PARAM_NUMERIC(flash_args.word->ival[0], persistent_data.param_word, MIN_PARAM_WORD, MAX_PARAM_WORD);
			return 0;
		}

		if (flash_args.dword->count>0) { //dword
			SET_PARAM_NUMERIC(flash_args.dword->ival[0], persistent_data.param_d_word, MIN_PARAM_D_WORD, MAX_PARAM_D_WORD);
			return 0;
		}

		if (flash_args.fl->count>0) { //float
			SET_PARAM_NUMERIC(flash_args.fl->dval[0], persistent_data.param_float, MIN_PARAM_FLOAT, MAX_PARAM_FLOAT);
			return 0;
		}

		if (flash_args.dbl->count>0) { //double
			SET_PARAM_NUMERIC(flash_args.dbl->dval[0], persistent_data.param_double, MIN_PARAM_DOUBLE, MAX_PARAM_DOUBLE);
			return 0;
		}

		if (flash_args.str->count>0) { //str
//			SET_PARAM_TEXT(flash_args.str->sval[0], persistent_data.param_string, PARAM_STRING_MAX_LEN);
			return 0;
		}

	}

	printf(HELP_FLASH);
	return 1;
}


static void register_flash_commands() {
	flash_args.set=arg_lit0("sS", ARG_SET, HINT_SET);
	flash_args.show=arg_lit0("wW", ARG_SHOW, HINT_SHOW);
	flash_args.store=arg_lit0("tT", ARG_STORE, HINT_STORE);
	flash_args.load=arg_lit0("lL", ARG_LOAD, HINT_LOAD);
	flash_args.stat=arg_lit0(NULL, ARG_STAT, HINT_STAT);
	flash_args.erase=arg_str0(NULL, ARG_ERASE, DATA_TYPE_ERASE, HINT_ERASE);

	flash_args.byte=arg_int0("bB", ARG_BYTE, DATA_TYPE_INT, HINT_BYTE);
	flash_args.word=arg_int0("dD", ARG_WORD, DATA_TYPE_INT, HINT_WORD);
	flash_args.dword=arg_int0("oO", ARG_D_WORD, DATA_TYPE_INT, HINT_D_WORD);

	flash_args.fl=arg_dbl0(NULL, ARG_FLOAT, DATA_TYPE_FLOAT, HINT_FLOAT);
	flash_args.dbl=arg_dbl0(NULL, ARG_DOUBLE, DATA_TYPE_FLOAT, HINT_DOUBLE);

	flash_args.str=arg_str0("rR", ARG_STRING, DATA_TYPE_STRING, HINT_STRING);
	flash_args.end=arg_end(0);

	const esp_console_cmd_t cmd={
			.command=CMD_FLASH,
			.help=HELP_FLASH,
			.hint=HINT_FLASH,
			.func=cmd_flash
	};

	ESP_ERROR_CHECK(esp_console_cmd_register(&cmd));
}

BaseType_t flashing_init() {
	bzero(&persistent_data, sizeof(persistent_data));
	esp_err_t err=load_params();
	if (err!=pdPASS)
			return err;

	register_flash_commands();
	return pdPASS;
}
