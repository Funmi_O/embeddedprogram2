/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "shell.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define DEB_CMD "switch"

void cmd_debounce(int argc, char **argv);

#define DEBOUNCE_COMMANDS \
{ .command_name=DEB_CMD, .function=cmd_debounce }

#define APPLICATION_TITLE "==== Switch Debounce demo ===="

#define DEBOUNCE_TIME 50

#define TASK_DELAY_MS 100
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

extern UART_HandleTypeDef huart2;

static shell_command_t shell_commands[] ={
		DEBOUNCE_COMMANDS,
		{NULL, NULL}
};

static shell_configuration_t shell_config ={
		.uart=&huart2,
		.commands=shell_commands,
		.stack_size=1024,
		.priority=osPriorityNormal,
		.rtc=NULL,
		.history_buffer_size=128,
		.app_title=APPLICATION_TITLE
};

static int btn_on=0;
static int btn_off=0;

int on_counter=0;
int off_counter=0;

static struct {
	uint8_t last_on;
	uint8_t last_off;
	float avg_stat_on;
	float avg_stat_off;
} switch_db;

enum Switch { LOW=0, HIGH=1 };

/* USER CODE END Variables */
/* Definitions for defaultTask */
osThreadId_t defaultTaskHandle;
const osThreadAttr_t defaultTask_attributes = {
  .name = "defaultTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};
/* Definitions for Button_task */
osThreadId_t Button_taskHandle;
const osThreadAttr_t Button_task_attributes = {
  .name = "Button_task",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void *argument);
void fn_button(void *argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
	sh_init(&shell_config);
  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of defaultTask */
  defaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &defaultTask_attributes);

  /* creation of Button_task */
  Button_taskHandle = osThreadNew(fn_button, NULL, &Button_task_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void *argument)
{
  /* USER CODE BEGIN StartDefaultTask */

  /* Infinite loop */
  for(;;)
  {
	  osDelay(1);
  }
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Header_fn_button */
/**
* @brief Function implementing the Button_task thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_fn_button */
void fn_button(void *argument)
{
  /* USER CODE BEGIN fn_button */
	TickType_t previous_tick_count=0;
	TickType_t Current_tick_count=0;
	uint8_t previous_state=0;
	uint8_t new_state  = 0;

		  /* Infinite loop */
  for(;;)
  {
	new_state = HAL_GPIO_ReadPin(B1_GPIO_Port, B1_Pin);

	HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, !new_state);

		if (new_state == previous_state) { 			//if the button isn't pressed

			if ((xTaskGetTickCount() - previous_tick_count) > DEBOUNCE_TIME) {
				Current_tick_count=xTaskGetTickCount();
				off_counter=0;
				on_counter=0;
			}
			goto wait;
		}

	  	if (new_state != previous_state)   { 		//if the button is pressed

	  		if (new_state == LOW) {

	  			if (new_state == HIGH) {
	  				btn_on++;
	  			}

	  			else {
	  				on_counter++;
	  				switch_db.last_on=on_counter+switch_db.last_on;
	  				switch_db.avg_stat_on=switch_db.last_on/on_counter;
	  			}
	  		}

	  		else if (new_state == HIGH) {

	  			if (new_state == LOW) {
	  				btn_off++;
	  			}
	  			off_counter++;
	  			switch_db.last_off=off_counter+switch_db.last_off;
	  			switch_db.avg_stat_off=switch_db.last_off/off_counter;
	  		}

	  		 vTaskDelay(TASK_DELAY_MS / portTICK_PERIOD_MS);
	  	}

	  	previous_state = new_state;	 /// btn idle state -> pressed
	  	previous_tick_count=xTaskGetTickCount();

wait:
	  	vTaskDelay(1 / portTICK_PERIOD_MS);
	}

  /* USER CODE END fn_button */
}



/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
	if (huart==&huart2)
	  sh_receive_complete(huart);
}

void cmd_debounce (int argc, char **argv) {
  sh_printf("button was last on : %d , button was last off: %d, average of the on state: %f, average of the off state: %f\r\n",
		  switch_db.last_on, switch_db.last_off,
		  switch_db.avg_stat_on, switch_db.avg_stat_off);
}
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
