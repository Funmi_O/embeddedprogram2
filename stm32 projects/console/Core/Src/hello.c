/*
 * hello.c
 *
 *  Created on: Sep 29, 2020
 *      Author: funmilayo75
 */
#include <string.h>
#include "cmsis_os.h"

#include "hello.h"
#include "stdbool.h"
#include "shell.h"

#define MIN_VALUE 42
#define MAX_VALUE 420
#define CMD_ARG_SET "set"
#define CMD_ARG_GET "get"

#define TASK_DELAY_MS 1000

static struct {
	bool initialized;
	uint16_t value;
} hello_control;

BaseType_t hello_init(){
	hello_control.initialized = true;
	hello_control.value = MIN_VALUE;
	return pdTRUE;
}

BaseType_t hello_deinit(){
	return pdTRUE;
}

void hello_function(const void * args){
	for(;;) {
		hello_control.value=hello_control.value < MAX_VALUE
				? hello_control.value+1
				: MIN_VALUE;
		osDelay(TASK_DELAY_MS);
	}
}

static void cmd_usage() {
	sh_printf("usage:%s [set <value>] | get]\r\n", HELLO_CMD);
}
void cmd_hello (int argc, char *argv[]){
	if (!hello_control.initialized) {
		sh_printf("hello is not initialized yet\r\n");
		return;
	}

	if (argc<1) {
		cmd_usage();
		return;
	}

	if (strcmp(argv[0], CMD_ARG_SET)==0) {
		if (argc!=2) {
			cmd_usage();
			return;
		}

		uint16_t nv = atoi(argv[1]);
		if (nv>=MIN_VALUE && nv<=MAX_VALUE) {
				hello_control.value=nv;
				sh_printf("Successfully changed to %d\r\n", nv);
				return;
		}
		sh_printf("value must be between %d and %d\r\n", MIN_VALUE, MAX_VALUE);
		return;
	}

	if (strcasecmp(argv[0], CMD_ARG_GET)==0) {
		sh_printf("current value is %d\r\n", hello_control.value);
		return;
	}

	sh_printf("Unknown command:%s\r\n", argv[0]);
	cmd_usage();
}

