/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "shell.h"
#include "analog.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define APPLICATION_TITLE "==== Analog/Digital Converter demo ===="


#define AD_SHELL_COMMANDS \
		{ .command_name=CMD_AD, .function=cmd_ad }
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

extern UART_HandleTypeDef huart2;

static shell_command_t shell_commands[] = {
		AD_SHELL_COMMANDS,
		{NULL,NULL}
};

static shell_configuration_t shell_config = {
		.uart=&huart2,
		.commands=shell_commands,
		.stack_size=1024,
		.priority=osPriorityNormal,
		.rtc=NULL,
		.history_buffer_size=128,
		.app_title=APPLICATION_TITLE
};
/* USER CODE END Variables */
/* Definitions for defaultTask */
osThreadId_t defaultTaskHandle;
const osThreadAttr_t defaultTask_attributes = {
  .name = "defaultTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};
/* Definitions for ADtask */
osThreadId_t ADtaskHandle;
const osThreadAttr_t ADtask_attributes = {
  .name = "ADtask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};
/* Definitions for VisTask */
osThreadId_t VisTaskHandle;
const osThreadAttr_t VisTask_attributes = {
  .name = "VisTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};
/* Definitions for mtx_ad */
osMutexId_t mtx_adHandle;
const osMutexAttr_t mtx_ad_attributes = {
  .name = "mtx_ad"
};

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void *argument);
void fn_AD_converter(void *argument);
void fn_AD_Vis(void *argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
	sh_init(&shell_config);

	ad_init();
  /* USER CODE END Init */
  /* Create the mutex(es) */
  /* creation of mtx_ad */
  mtx_adHandle = osMutexNew(&mtx_ad_attributes);

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of defaultTask */
  defaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &defaultTask_attributes);

  /* creation of ADtask */
  ADtaskHandle = osThreadNew(fn_AD_converter, NULL, &ADtask_attributes);

  /* creation of VisTask */
  VisTaskHandle = osThreadNew(fn_AD_Vis, NULL, &VisTask_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void *argument)
{
  /* USER CODE BEGIN StartDefaultTask */
  /* Infinite loop */
  for(;;)
  {
	HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
    osDelay(300);
  }
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Header_fn_AD_converter */
/**
* @brief Function implementing the ADtask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_fn_AD_converter */
void fn_AD_converter(void *argument)
{
  /* USER CODE BEGIN fn_AD_converter */
  /* Infinite loop */

 ad_task_fn(NULL);

  /* USER CODE END fn_AD_converter */
}

/* USER CODE BEGIN Header_fn_AD_Vis */
/**
* @brief Function implementing the VisTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_fn_AD_Vis */
void fn_AD_Vis(void *argument)
{
  /* USER CODE BEGIN fn_AD_Vis */
  /* Infinite loop */
	ad_vis_fn(NULL);

  /* USER CODE END fn_AD_Vis */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
  if (huart == &huart2) {
	  	  sh_receive_complete(huart);
	  	  return;
  }
}
/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
