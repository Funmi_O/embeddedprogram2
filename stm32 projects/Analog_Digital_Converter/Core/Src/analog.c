/*
 * analog.c
 *
 *  Created on: Oct 19, 2020
 *      Author: funmilayo75
 */

#define DEBUG_MODULE
#include "analog.h"
#include "shell.h"
#include <string.h>
#include "semphr.h"

#define AD_MAX_VALUE (4096)

#define AD_MIN_VALUE (1500)

#define NOTIFY_WAIT_MS (100)

#define CONV_INTERVAL_MS (20)

#define AD_TASK_SLEEP_TIME_MS (100)

#define VIS_TASK_SLEEP_TIME_MS (100)

#define EVT_AD_CONV_END (1<<0)

#define EVT_AD_CONV_START (1<<1)

#define EVT_AD_CONV_STOP (1<<2)

#define EVT_AD_VIS_START (1<<3)

#define EVT_AD_VIS_STOP (1<<4)

#define EVT_AD_MASK ((EVT_AD_CONV_END) | (EVT_AD_CONV_START) | (EVT_AD_CONV_STOP))

#define EVT_VIS_MASK ((EVT_AD_VIS_START) | (EVT_AD_VIS_STOP))

#define VISUALIZATION_MAX_VALUE (50)

#define MOVING_AVERAGE_QUEUE_SIZE (10)

#define MOVING_HYSTERESIS_DELTA (10)

typedef struct {
	uint32_t start_error;
	uint32_t stop_error;
	uint32_t conv_success;
	uint32_t conv_error;
} ad_statistics_t;

typedef struct {
	uint32_t delta;
	uint32_t hys_min;
	uint32_t hys_max;
} moving_hysteresis_t;

typedef struct {
	uint32_t queue[MOVING_AVERAGE_QUEUE_SIZE];
	uint32_t queue_index;
	uint32_t average;
} moving_average_t;

typedef struct {
	volatile uint8_t running;
	volatile uint8_t vis_running;
	uint32_t raw_value;
	ad_statistics_t statistics;
	moving_average_t moving_average;
	moving_hysteresis_t moving_hysteresis;
} ad_task_ctrl_t;

static ad_task_ctrl_t ad_task_ctrl;
extern ADC_HandleTypeDef hadc1;
extern osThreadId_t ADtaskHandle;
extern osThreadId_t VisTaskHandle;
extern osMutexId_t mtx_adHandle;

BaseType_t ad_init(){
	bzero(&ad_task_ctrl, sizeof(ad_task_ctrl_t));
	ad_task_ctrl.moving_hysteresis.delta=MOVING_HYSTERESIS_DELTA;
	ad_task_ctrl.moving_hysteresis.hys_min=0;
	ad_task_ctrl.moving_hysteresis.hys_max=MOVING_HYSTERESIS_DELTA;
	return pdPASS;
};

BaseType_t ad_deinit(){
	return pdPASS;
}

//IT entry point
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc) {
	++ad_task_ctrl.statistics.conv_success;
	BaseType_t xHigherPriorityTaskWoken =pdFALSE;
	xTaskNotifyFromISR(ADtaskHandle, EVT_AD_CONV_END, eSetBits, &xHigherPriorityTaskWoken);
	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

//conversion error it
void HAL_ADC_ErrorCallback(ADC_HandleTypeDef *hadc) {
	HAL_ADC_Stop_DMA(&hadc1);
	++ad_task_ctrl.statistics.conv_error;
}

static void start_ad_conversion() {
	BaseType_t result = HAL_ADC_Start_DMA(&hadc1, &ad_task_ctrl.raw_value, 1);
	if (result != HAL_OK)
		++ad_task_ctrl.statistics.start_error;
	else
		ad_task_ctrl.running=1;
}

static uint32_t add_moving_hysteresis_value(uint32_t v) {
	if (v>ad_task_ctrl.moving_hysteresis.hys_max) {		// out of range in positive direction
		ad_task_ctrl.moving_hysteresis.hys_max += ad_task_ctrl.moving_hysteresis.delta;
		ad_task_ctrl.moving_hysteresis.hys_min += ad_task_ctrl.moving_hysteresis.delta;
		return ad_task_ctrl.moving_hysteresis.hys_max;
	}
	if (v<ad_task_ctrl.moving_hysteresis.hys_min) {		// out of range in negative direction
		ad_task_ctrl.moving_hysteresis.hys_min -= ad_task_ctrl.moving_hysteresis.delta;
		ad_task_ctrl.moving_hysteresis.hys_max -= ad_task_ctrl.moving_hysteresis.delta;
		return ad_task_ctrl.moving_hysteresis.hys_min;
	}
	return v;
}

static void add_moving_average_value(uint32_t v) {
	ad_task_ctrl.moving_average.queue[ad_task_ctrl.moving_average.queue_index] = v;
	ad_task_ctrl.moving_average.queue_index = (ad_task_ctrl.moving_average.queue_index+1) % MOVING_AVERAGE_QUEUE_SIZE;
	uint32_t sum = 0;

	for (uint8_t i=0; i<MOVING_AVERAGE_QUEUE_SIZE; ++i){
		sum += ad_task_ctrl.moving_average.queue[i];

		ad_task_ctrl.moving_average.average = sum / MOVING_AVERAGE_QUEUE_SIZE;
	}
}

void ad_task_fn(const void *par){
	uint32_t notified_value;
	BaseType_t result;
	for (;;) {
		result = xTaskNotifyWait(EVT_AD_MASK, EVT_AD_MASK, &notified_value, pdMS_TO_TICKS(NOTIFY_WAIT_MS));

		pr_debug("Leave A/D task notify, it is: 0x%x", notified_value);

		if (notified_value & EVT_AD_CONV_STOP) {
			ad_task_ctrl.running=0;
			result=HAL_ADC_Stop_DMA(&hadc1);
			if (result != HAL_OK) {
				++ad_task_ctrl.statistics.conv_error;
				pr_err("AD stop error");
			}
		}

		if (notified_value & EVT_AD_CONV_START) {
			start_ad_conversion();
		}

		if (notified_value & EVT_AD_CONV_END) {

			add_moving_average_value(add_moving_hysteresis_value(ad_task_ctrl.raw_value));
			//missing info

			++ad_task_ctrl.statistics.conv_success;
			osDelay(CONV_INTERVAL_MS);
			if(!ad_task_ctrl.running)
				continue;
			start_ad_conversion();
		}
	}
};

uint8_t ad_get (uint32_t *result, TickType_t wait) {
	assert_param(result);
	if (xSemaphoreTake(mtx_adHandle, wait)==pdTRUE) {
		*result = ad_task_ctrl.moving_average.average;
		xSemaphoreGive(mtx_adHandle);
		return pdPASS;
	}
	return pdFALSE;
}

static inline void vis_value(char *buffer, uint32_t v) {
	bzero(buffer, VISUALIZATION_MAX_VALUE+1);
	memset(buffer, '=', v);
	pr_info(buffer);
}

static inline uint32_t min (uint32_t a, uint32_t b) {
	return a>b?a:b;
}

void ad_vis_fn(const void *par) {
	char visual_buffer[VISUALIZATION_MAX_VALUE+1];

	for(;;) {
		uint32_t notified_value;
		xTaskNotifyWait(0, EVT_VIS_MASK, &notified_value, pdMS_TO_TICKS(NOTIFY_WAIT_MS));

		pr_debug("Leave A/D vis_task notify, it is: 0x%x", notified_value);

		if (notified_value & EVT_AD_VIS_START) {
					ad_task_ctrl.vis_running = 1;
		}

		if (notified_value & EVT_AD_VIS_STOP) {
			ad_task_ctrl.vis_running=0;
			vis_value(visual_buffer, 0);
			goto tsk_wait;
		}

		if (ad_task_ctrl.vis_running) {
			uint32_t current=0;
			if (!ad_get(&current, osWaitForever)) {
				goto tsk_wait;
			}
			current = (current-AD_MIN_VALUE) * VISUALIZATION_MAX_VALUE / (AD_MAX_VALUE-AD_MIN_VALUE);
			uint32_t size = min(VISUALIZATION_MAX_VALUE, current);
			vis_value(visual_buffer, size);
		}


tsk_wait:
		osDelay(VIS_TASK_SLEEP_TIME_MS);
	}
}


#define CMD_HELP "help"
#define CMD_STAT "stat"
#define CMD_STOP "stop"
#define CMD_START "start"
#define CMD_VIS "vis"

static void cmd_usage() {
	sh_printf("usage: %s <help|stat|start|vis<start|stop>> \r\n", CMD_AD);
}

void cmd_ad (int argc, char **argv){
	if (argc<=0 || strcasecmp(CMD_HELP, argv[0])==0) {
		cmd_usage();
		return;
	}

	if (strcasecmp(CMD_STAT, argv[0])==0) {
		sh_printf("State is %s\r\n", ad_task_ctrl.running ? "started" : "stopped");
		sh_printf("conversions:%d, conv_errors:%d, start errors:%d, stop errors:%d\r\n",
				ad_task_ctrl.statistics.conv_success,
				ad_task_ctrl.statistics.conv_error,
				ad_task_ctrl.statistics.start_error,
				ad_task_ctrl.statistics.stop_error);
		sh_printf("Current value is: %d, raw value is: %d\r\n",
				ad_task_ctrl.moving_average.average, ad_task_ctrl.raw_value);
		return;
	}

	if (strcasecmp(CMD_START, argv[0])==0) {
		if (ad_task_ctrl.running) {
			sh_printf("AD Task already started\r\n");
			return;
		}
		xTaskNotify(ADtaskHandle, EVT_AD_CONV_START, eSetBits);
		sh_printf("AD Task is starting\r\n");
		return;
	}

	if (strcasecmp(CMD_STOP, argv[0])==0) {
		if (!ad_task_ctrl.running) {
			sh_printf("AD Task already stopped\r\n");
			return;
		}
		xTaskNotify(ADtaskHandle, EVT_AD_CONV_STOP, eSetBits);
				sh_printf("AD Task is stopping\r\n");
				return;
	}

	if (argc==2 && strcasecmp(CMD_VIS, argv[0])==0){
		if (strcasecmp(CMD_START, argv[1])==0) {
			if (ad_task_ctrl.vis_running) {
				sh_printf("Visualization already started\r\n");
				return;
			}
			xTaskNotify(VisTaskHandle, EVT_AD_VIS_START, eSetBits);
			sh_printf("Visualization is starting\r\n");
			return;
		}

		if (strcasecmp(CMD_STOP, argv[1])==0) {
			if (!ad_task_ctrl.vis_running) {
				sh_printf("Visualization already stopped\r\n");
				return;
			}
			xTaskNotify(VisTaskHandle, EVT_AD_VIS_STOP, eSetBits);
			sh_printf("Visualization is stopping\r\n");
			return;
		}
	}

	cmd_usage();
};
