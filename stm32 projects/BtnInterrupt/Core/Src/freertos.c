/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#undef DEBUG_MODULE
#include "shell.h"
#include "event_groups.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define BTN_EVT_MASK (1<<0)

#define BTN_CMD "btn"

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */
static EventGroupHandle_t events;

extern UART_HandleTypeDef huart2;

void cmd_button (int argc, char **argv);

#define BTN_COMMANDS \
{ .command_name=BTN_CMD, .function=cmd_button }

#define APPLICATION_TITLE "==== Button Interrupt demo ===="

static shell_command_t shell_commands[] = {
		BTN_COMMANDS,
		{NULL, NULL}
};

static shell_configuration_t shell_config ={
		.uart = &huart2,
		.commands=shell_commands,
		.stack_size=1024,
		.priority = osPriorityNormal,
		.rtc=NULL,
		.history_buffer_size=128,
		.app_title = APPLICATION_TITLE
};

static int counter = 0;

/* USER CODE END Variables */
/* Definitions for defaultTask */
osThreadId_t defaultTaskHandle;
const osThreadAttr_t defaultTask_attributes = {
  .name = "defaultTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};
/* Definitions for ButtonTask */
osThreadId_t ButtonTaskHandle;
const osThreadAttr_t ButtonTask_attributes = {
  .name = "ButtonTask",
  .priority = (osPriority_t) osPriorityNormal,
  .stack_size = 128 * 4
};

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void *argument);
void fn_button(void *argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
	events = xEventGroupCreate();
	assert_param(events);

	sh_init(&shell_config);
  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of defaultTask */
  defaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &defaultTask_attributes);

  /* creation of ButtonTask */
  ButtonTaskHandle = osThreadNew(fn_button, NULL, &ButtonTask_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void *argument)
{
  /* USER CODE BEGIN StartDefaultTask */
  /* Infinite loop */
  for(;;)
  {
	//HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
    osDelay(300);
  }
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Header_fn_button */
/**
* @brief Function implementing the ButtonTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_fn_button */
void fn_button(void *argument)
{
  /* USER CODE BEGIN fn_button */
  /* Infinite loop */
	EventBits_t bits;
	const TickType_t ticks_to_wait = 100 / portTICK_PERIOD_MS;

  for(;;)
  {
	bits = xEventGroupWaitBits(events, BTN_EVT_MASK, pdTRUE, pdFALSE, ticks_to_wait);
	if (bits==BTN_EVT_MASK) {
		HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
		++counter;
	}
  }
  /* USER CODE END fn_button */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) { 		//interrupt
	if (GPIO_Pin!=B1_Pin)
		return;

	BaseType_t xHigherPriorityTaskWoken;
	BaseType_t result = xEventGroupSetBitsFromISR(events, BTN_EVT_MASK, &xHigherPriorityTaskWoken);
	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
	if(huart==&huart2)
		sh_receive_complete(huart);
}

void cmd_button(int argc, char **argv) {
	sh_printf("button counter:%d\r\n", counter);
}

/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
